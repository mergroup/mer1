<!DOCTYPE html>
<html lang="en">
	@include('site.layouts.head')
<body>
	@include('site.layouts.header')

	@yield('content')

	@include('site.layouts.footer')

	@include('site.layouts.footermobile')

	@include('site.layouts.js')

</body>
</html>
