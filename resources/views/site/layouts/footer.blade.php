<footer id="footer" class="footer style-03">
	<div class="section-001 section-020">
		<div class="container">
			<div class="kaycee-newsletter style-03">
				<div class="newsletter-inner">
					<div class="newsletter-info">
						<div class="newsletter-wrap">
							<h3 class="title">Newsletter<span></span></h3>
							<h4 class="subtitle">Get Discount 30% Off</h4>
							<p class="desc">Nam sed felis at eros blandit ultrices et quis quam. In sit amet molestie
								lectus.</p>
						</div>
					</div>
					<div class="newsletter-form-wrap">
						<div class="newsletter-form-inner">
							<input class="email email-newsletter" name="email" placeholder="Enter your email ..."
								   type="email">
							<a href="#" class="button btn-submit submit-newsletter">
								<span class="text">Subscribe</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kaycee-socials style-02">
				<div class="content-socials">
					<ul class="socials-list">
						<li>
							<a href="https://facebook.com/" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/" target="_blank">
								<i class="fa fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li>
							<a href="https://www.pinterest.com/" target="_blank">
								<i class="fa fa-pinterest-p"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="section-021">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p>© Copyright 2021 <a href="#">MER</a>. All Rights Reserved.</p>
				</div>
				<div class="col-md-6">
					<div class="kaycee-listitem style-03">
						<div class="listitem-inner">
							<ul class="listitem-list">
								<li>
									<a href="#" target="_self">
										Contact </a>
								</li>
								<li>
									<a href="#" target="_self">
										Term &amp; Conditions </a>
								</li>
								<li>
									<a href="#" target="_self">
										Shipping </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
