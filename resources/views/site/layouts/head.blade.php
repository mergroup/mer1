<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png"/>
	<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/animate.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/chosen.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/pe-icon-7-stroke.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.scrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/lightbox.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/slick.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/megamenu.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/dreaming-attribute.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ 'MER' }}</title>
</head>
