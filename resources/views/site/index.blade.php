@extends('site.layouts.layoutsite')

@section('content')
	<div class="fullwidth-template">
		<div class="slide-home-03">
			<div class="response-product product-list-owl owl-slick equal-container better-height"
				 data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:0,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:1,&quot;rows&quot;:1}"
				 data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}}]">
				<div class="slide-wrap">
					<img src="assets/images/slide31.jpg" alt="image">
					<div class="slide-info">
						<div class="container">
							<div class="slide-inner">
								<h1>The Summer </h1>
								<h5>Lookbook</h5>
								<h2><span>Beauty & </span>Cool</h2>
								<a href="#">Shop now</a>
							</div>
						</div>
					</div>
				</div>
				<div class="slide-wrap">
					<img src="assets/images/slide32.jpg" alt="image">
					<div class="slide-info">
						<div class="container">
							<div class="slide-inner">
								<h1>The Spring </h1>
								<h5>New Arrivals</h5>
								<h2><span>Natural &</span> Glamour</h2>
								<a href="#">Shop now</a>
							</div>
						</div>
					</div>
				</div>
				<div class="slide-wrap">
					<img src="assets/images/slide33.jpg" alt="image">
					<div class="slide-info">
						<div class="container">
							<div class="slide-inner">
								<h1>The Autumn </h1>
								<h5>Best Seller</h5>
								<h2><span>SS2020</span> Collection</h2>
								<a href="#">Shop now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-017">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="kaycee-banner style-08 left-center">
						<div class="banner-inner">
							<figure class="banner-thumb">
								<img src="assets/images/banner31.jpg"
									 class="attachment-full size-full" alt="img"></figure>
							<div class="banner-info ">
								<div class="banner-content">
									<div class="title-wrap">
										<h6 class="title">
											Best Seller</h6>
									</div>
									<div class="cate">
										Check out our your<br>
										jewelry collection now!
									</div>
									<div class="button-wrap">
										<a class="button" target="_self" href="#"><span>Shop now</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="kaycee-banner style-09 left-center">
						<div class="banner-inner">
							<figure class="banner-thumb">
								<img src="assets/images/banner32.jpg"
									 class="attachment-full size-full" alt="img"></figure>
							<div class="banner-info ">
								<div class="banner-content">
									<div class="title-wrap">
										<div class="banner-label">
											End this weekend
										</div>
										<h6 class="title">
											Big Sale<br>
											75% Off </h6>
									</div>
									<div class="cate">
										Promo Code: <strong>KAYCEE</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="kaycee-banner style-10 left-center">
						<div class="banner-inner">
							<figure class="banner-thumb">
								<img src="assets/images/banner33.jpg"
									 class="attachment-full size-full" alt="img"></figure>
							<div class="banner-info ">
								<div class="banner-content">
									<div class="title-wrap">
										<h6 class="title">
											Lookbook</h6>
									</div>
									<div class="cate">
										New Jewelry Collections<br>
										Summer Lookbook
									</div>
									<div class="button-wrap">
										<a class="button" target="_self" href="#"><span>Shop now</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-001 pb-0">
			<div class="container">
				<div class="kaycee-heading style-01">
					<div class="heading-inner">
						<h3 class="title">
							Top Trend <span></span></h3>
						<div class="subtitle">
							Browse our website for the hottest items now.
						</div>
					</div>
				</div>
				<div class="kaycee-products style-04">
					<div class="response-product product-list-grid row auto-clear equal-container better-height ">
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-25 product type-product status-publish has-post-thumbnail product_cat-light product_cat-chair product_cat-specials product_tag-light product_tag-sock first instock sale featured shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro151-1-270x350.jpg"
											 alt="Modern Platinum" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onsale"><span class="number">-11%</span></span>
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Modern Platinum</a>
									</h3>
									<span class="price"><del><span class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>89.00</span></del> <ins><span
												class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>79.00</span></ins></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-23 product type-product status-publish has-post-thumbnail product_cat-chair product_cat-lamp product_cat-sofas product_tag-hat  instock shipping-taxable purchasable product-type-variable has-default-attributes">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro171-1-270x350.jpg"
											 alt="Splendid Diamond" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<form class="variations_form cart">
										<table class="variations">
											<tbody>
											<tr>
												<td class="value">
													<select title="box_style" data-attributetype="box_style"
															data-id="pa_color"
															class="attribute-select " name="attribute_pa_color"
															data-attribute_name="attribute_pa_color"
															data-show_option_none="yes">
														<option data-type="" data-pa_color="" value="">Choose an
															option
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#ffe59e" value="pink"
																class="attached enabled">Pink
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#a825ea" value="purple"
																class="attached enabled">Purple
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#b6b8ba" value="red"
																class="attached enabled">Red
														</option>
													</select>
													<div class="data-val attribute-pa_color"
														 data-attributetype="box_style">
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #3155e2;" data-value="blue">
															</span>
														</label>
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #52b745;" data-value="green">
															</span>
														</label>
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #ffe59e;" data-value="pink">
															</span>
														</label>
													</div>
													<a class="reset_variations" href="#"
													   style="visibility: hidden;">Clear</a>
												</td>
											</tr>
											</tbody>
										</table>
									</form>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Select
												options</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Splendid Diamond</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>105.00</span> – <span
											class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>110.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-32 product type-product status-publish has-post-thumbnail product_cat-light product_cat-chair product_cat-sofas product_tag-hat product_tag-sock last instock sale featured shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro71-1-270x350.jpg"
											 alt="Gold Chain" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onsale"><span class="number">-18%</span></span>
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Gold Chain</a>
									</h3>
									<span class="price"><del><span class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>109.00</span></del> <ins><span
												class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>89.00</span></ins></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-20 product type-product status-publish has-post-thumbnail product_cat-light product_cat-new-arrivals product_cat-specials product_tag-table product_tag-hat product_tag-sock first instock sale featured shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro201-1-270x350.jpg"
											 alt="Riona Pearl" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onsale"><span class="number">-7%</span></span>
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Riona Pearl</a>
									</h3>
									<span class="price"><del><span class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>150.00</span></del> <ins><span
												class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>139.00</span></ins></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-36 product type-product status-publish has-post-thumbnail product_cat-table product_cat-bed product_tag-light product_tag-table product_tag-sock  instock sale shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro51012-1-270x350.jpg"
											 alt="Dazzling Earrings" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onsale"><span class="number">-21%</span></span>
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Dazzling Earrings</a>
									</h3>
									<span class="price"><del><span class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>125.00</span></del> <ins><span
												class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>99.00</span></ins></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-49 product type-product status-publish has-post-thumbnail product_cat-light product_cat-bed product_cat-sofas product_tag-multi product_tag-lamp last instock shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro302-270x350.jpg"
											 alt="Stylised Studs" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Stylised Studs</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>79.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-93 product type-product status-publish has-post-thumbnail product_cat-light product_cat-table product_cat-new-arrivals product_tag-table product_tag-sock first instock shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro13-1-270x350.jpg"
											 alt="Stud Earrings" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Stud Earrings</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>109.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-28 product type-product status-publish has-post-thumbnail product_cat-light product_cat-chair product_cat-sofas product_tag-light product_tag-sock  instock sale featured shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro1211-2-270x350.jpg"
											 alt="Gold Bracelet" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onsale"><span class="number">-14%</span></span>
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Gold Bracelet</a>
									</h3>
									<span class="price"><del><span class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>138.00</span></del> <ins><span
												class="kaycee-Price-amount amount"><span
													class="kaycee-Price-currencySymbol">$</span>119.00</span></ins></span>
									<div class="rating-wapper ">
										<div class="star-rating"><span style="width:100%">Rated <strong
													class="rating">5.00</strong> out of 5</span>
										</div>
										<span class="review">(1)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-22 product type-product status-publish has-post-thumbnail product_cat-table product_cat-bed product_cat-lamp product_tag-table product_tag-hat product_tag-sock last instock featured downloadable shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro181-2-270x350.jpg"
											 alt="Mesmerising Gold" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Mesmerising Gold</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>98.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-30 product type-product status-publish has-post-thumbnail product_cat-light product_cat-bed product_cat-specials product_tag-light product_tag-table product_tag-sock first instock featured downloadable shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro101-1-270x350.jpg"
											 alt="Smooth Sleek" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Smooth Sleek</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>60.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-35 product type-product status-publish has-post-thumbnail product_cat-chair product_cat-new-arrivals product_cat-lamp product_tag-light product_tag-hat product_tag-sock  instock shipping-taxable purchasable product-type-simple">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro41-1-270x350.jpg"
											 alt="Stylised Studs" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to
												cart</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Stylised Studs</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>134.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
						<div class="product-item best-selling style-04 rows-space-30 col-bg-3 col-xl-3 col-lg-4 col-md-4 col-sm-6 col-ts-6 post-24 product type-product status-publish has-post-thumbnail product_cat-chair product_cat-table product_cat-new-arrivals product_tag-light product_tag-hat product_tag-sock last instock featured shipping-taxable purchasable product-type-variable has-default-attributes">
							<div class="product-inner tooltip-top tooltip-all-top">
								<div class="product-thumb">
									<a class="thumb-link" href="#">
										<img class="img-responsive"
											 src="assets/images/apro161-1-270x350.jpg"
											 alt="Gold Chain" width="270" height="350">
									</a>
									<div class="flash">
										<span class="onnew"><span class="text">New</span></span></div>
									<form class="variations_form cart">
										<table class="variations">
											<tbody>
											<tr>
												<td class="value">
													<select title="box_style" data-attributetype="box_style"
															data-id="pa_color"
															class="attribute-select " name="attribute_pa_color"
															data-attribute_name="attribute_pa_color"
															data-show_option_none="yes">
														<option data-type="" data-pa_color="" value="">Choose an
															option
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#3155e2" value="blue"
																class="attached enabled">Blue
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#52b745" value="green"
																class="attached enabled">Green
														</option>
														<option data-width="30" data-height="30" data-type="color"
																data-pa_color="#ffe59e" value="pink"
																class="attached enabled">Pink
														</option>
													</select>
													<div class="data-val attribute-pa_color"
														 data-attributetype="box_style">
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #3155e2;" data-value="blue">
															</span>
														</label>
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #52b745;" data-value="green">
															</span>
														</label>
														<label>
															<input type="radio" name="color">
															<span class="change-value color"
																  style="background: #ffe59e;" data-value="pink">
															</span>
														</label>
													</div>
													<a class="reset_variations" href="#"
													   style="visibility: hidden;">Clear</a>
												</td>
											</tr>
											</tbody>
										</table>
									</form>
									<div class="group-button">
										<div class="add-to-cart">
											<a href="#"
											   class="button product_type_simple add_to_cart_button ajax_add_to_cart">Select
												options</a>
										</div>
										<a href="#" class="button yith-wcqv-button">Quick View</a>
										<div class="kaycee product compare-button">
											<a href="#" class="compare button">Compare</a></div>
										<div class="yith-wcwl-add-to-wishlist">
											<div class="yith-wcwl-add-button show">
												<a href="#" class="add_to_wishlist">Add to Wishlist</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-info">
									<h3 class="product-name product_title">
										<a href="#">Gold Chain</a>
									</h3>
									<span class="price"><span class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>45.00</span> – <span
											class="kaycee-Price-amount amount"><span
												class="kaycee-Price-currencySymbol">$</span>54.00</span></span>
									<div class="rating-wapper nostar">
										<div class="star-rating"><span style="width:0%">Rated <strong
													class="rating">0</strong> out of 5</span></div>
										<span class="review">(0)</span></div>
								</div>
							</div>
						</div>
					</div>
					<!-- OWL Products -->
					<div class="shop-all">
						<a target=" _blank" href="#">Discovery All</a>
					</div>
				</div>
			</div>
		</div>
		<div class="section-0181">
			<div class="container">
				<div class="kaycee-heading style-01">
					<div class="heading-inner">
						<h3 class="title">
							What's Clients Says? <span></span></h3>
						<div class="subtitle">
							Lorem ipsum dolor sit amet consectetur
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-018">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="kaycee-slide">
							<div class="owl-slick equal-container better-height"
								 data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:0,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:1,&quot;vertical&quot;:false,&quot;verticalSwiping&quot;:false,&quot;rows&quot;:1}"
								 data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}}]">
								<div class="kaycee-testimonial style-01">
									<div class="testimonial-inner">
										<div class="thumb">
											<img src="assets/images/avatar1.png"
												 class="attachment-full size-full" alt="img" width="97"
												 height="97"></div>
										<p class="desc">Lorem ipsum dolor sit amet consectetur elit sagittis,
											quisque ut integer penatibus eleifend erat porttitor,
											viverra tristique dapibus fermentum montes aptent enim magnis nec a neque. </p>
										<div class="testimonial-info">
											<div class="intro">
												<h3 class="name">
													<a href="#" target="_self" tabindex="0">
														Charley Pratt </a>
												</h3>
												<div class="position">
													Customer
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kaycee-testimonial style-01">
									<div class="testimonial-inner">
										<div class="thumb">
											<img src="assets/images/avatar3.png"
												 class="attachment-full size-full" alt="img" width="97"
												 height="97"></div>
										<p class="desc">Lorem ipsum dolor sit amet consectetur elit sagittis,
											quisque ut integer penatibus eleifend erat porttitor,
											viverra tristique dapibus fermentum montes aptent enim magnis nec a neque. </p>
										<div class="testimonial-info">
											<div class="intro">
												<h3 class="name">
													<a href="#" target="_self" tabindex="-1">
														Nadia Pugh </a>
												</h3>
												<div class="position">
													Customer
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kaycee-testimonial style-01">
									<div class="testimonial-inner">
										<div class="thumb">
											<img src="assets/images/avatar2.png"
												 class="attachment-full size-full" alt="img" width="97"
												 height="97"></div>
										<p class="desc">Lorem ipsum dolor sit amet consectetur elit sagittis,
											quisque ut integer penatibus eleifend erat porttitor,
											viverra tristique dapibus fermentum montes aptent enim magnis nec a neque. </p>
										<div class="testimonial-info">
											<div class="intro">
												<h3 class="name">
													<a href="#" target="_self" tabindex="-1">
														Troy Bryant </a>
												</h3>
												<div class="position">
													Customer
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-001 pt-0">
			<div class="container">
				<div class="kaycee-heading style-01">
					<div class="heading-inner">
						<h3 class="title">
							From Our Blog<span></span></h3>
						<div class="subtitle">
							Lorem ipsum dolor sit amet consectetur
						</div>
					</div>
				</div>
				<div class="kaycee-blog style-02">
					<div class="blog-list-owl owl-slick equal-container better-height"
						 data-slick="{&quot;arrows&quot;:true,&quot;slidesMargin&quot;:30,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:3,&quot;rows&quot;:1}"
						 data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
						<article
							class="post-item post-grid rows-space-0 post-195 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-table category-life-style tag-light tag-life-style">
							<div class="post-inner">
								<div class="post-thumb">
									<a href="#" tabindex="0">
										<img src="assets/images/blogpost1-370x330.jpg"
											 class="img-responsive attachment-370x330 size-370x330" alt="img"
											 width="370" height="330"> </a>
								</div>
								<div class="post-content">
									<div class="post-info">
										<div class="title-date">
											<h2 class="post-title">
												<a href="#" tabindex="0">Not your ordinary multi service.</a>
											</h2>
										</div>
										<div class="post-meta">
											<div class="post-author">
												<a href="#"> admin </a>
											</div>
											<div class="date">
												<a href="#">June 03, 2020</a>
											</div>
										</div>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sodales
										quisque litora dapibus primis lacinia
									</div>
								</div>
							</div>
						</article>
						<article
							class="post-item post-grid rows-space-0 post-192 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-fashion category-multi category-life-style tag-light tag-fashion tag-multi">
							<div class="post-inner">
								<div class="post-thumb">
									<a href="#" tabindex="0">
										<img src="assets/images/blogpost5-370x330.jpg"
											 class="img-responsive attachment-370x330 size-370x330" alt="img" width="370"
											 height="330">
									</a>
								</div>
								<div class="post-content">
									<div class="post-info">
										<div class="title-date">
											<h2 class="post-title">
												<a href="#" tabindex="0">We bring you the best by working</a></h2>
										</div>
										<div class="post-meta">
											<div class="post-author">
												<a href="#"> admin </a>
											</div>
											<div class="date">
												<a href="#">June 03, 2020</a>
											</div>
										</div>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sodales
										quisque litora dapibus primis lacinia
									</div>
								</div>
							</div>
						</article>
						<article
							class="post-item post-grid rows-space-0 post-189 post type-post status-publish format-video has-post-thumbnail hentry category-table category-life-style tag-multi tag-life-style post_format-post-format-video">
							<div class="post-inner">
								<div class="post-thumb">
									<a href="#"
									   tabindex="0">
										<img src="assets/images/blogpost9-370x330.jpg"
											 class="img-responsive attachment-370x330 size-370x330" alt="img"
											 width="370" height="330"> </a>
								</div>
								<div class="post-content">
									<div class="post-info">
										<div class="title-date">
											<h2 class="post-title"><a
													href="#"
													tabindex="0">We design functional best multi</a></h2>
										</div>
										<div class="post-meta">
											<div class="post-author">
												<a href="#"> admin </a>
											</div>
											<div class="date">
												<a href="#">June 03, 2020</a>
											</div>
										</div>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sodales
										quisque litora dapibus primis lacinia
									</div>
								</div>
							</div>
						</article>
						<article
							class="post-item post-grid rows-space-0 post-186 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-life-style tag-life-style">
							<div class="post-inner">
								<div class="post-thumb">
									<a href="#" tabindex="-1">
										<img src="assets/images/blogpost4-370x330.jpg"
											 class="img-responsive attachment-370x330 size-370x330" alt="img" width="370"
											 height="330">
									</a>
								</div>
								<div class="post-content">
									<div class="post-info">
										<div class="title-date">
											<h2 class="post-title">
												<a href="#" tabindex="-1">The child is swimming with a buoy</a>
											</h2>
										</div>
										<div class="post-meta">
											<div class="post-author">
												<a href="#"> admin </a>
											</div>
											<div class="date">
												<a href="#">June 03, 2020</a>
											</div>
										</div>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sodales
										quisque litora dapibus primis lacinia
									</div>
								</div>
							</div>
						</article>
						<article
							class="post-item post-grid rows-space-0 post-183 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-fashion tag-light tag-multi">
							<div class="post-inner">
								<div class="post-thumb">
									<a href="#" tabindex="-1">
										<img src="assets/images/blogpost2-370x330.jpg"
											 class="img-responsive attachment-370x330 size-370x330" alt="img"
											 width="370" height="330"> </a>
								</div>
								<div class="post-content">
									<div class="post-info">
										<div class="title-date">
											<h2 class="post-title"><a
													href="#"
													tabindex="-1">Collection hiding beside beige wall</a></h2>
										</div>
										<div class="post-meta">
											<div class="post-author">
												<a href="#"> admin </a>
											</div>
											<div class="date">
												<a href="#">June 03, 2020</a>
											</div>
										</div>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sodales
										quisque litora dapibus primis lacinia
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
		<div class="section-043 section-019">
			<div class="kaycee-heading style-01">
				<div class="heading-inner">
					<h3 class="title">
						Follow Us <span></span></h3>
					<div class="subtitle">
						@kayceestore
					</div>
				</div>
			</div>
			<div class="kaycee-instagram style-01">
				<div class="instagram-owl owl-slick"
					 data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:0,&quot;dots&quot;:false,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:6,&quot;rows&quot;:1}"
					 data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesMargin&quot;:&quot;0&quot;}}]">
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta1.jpg" alt="Home 03">
							<span class="instagram-info">
							 <span class="social-wrap">
								<span class="social-info">1
									<i class="pe-7s-chat"></i>
								</span>
								<span class="social-info">0
									<i class="pe-7s-like2"></i>
								</span>
							</span>
						</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta2.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta3.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta4.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta5.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta6.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta7.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
					<div class="rows-space-0">
						<a target="_blank" href="#" class="item  " tabindex="0">
							<img class="img-responsive lazy" src="assets/images/insta8.jpg" alt="Home 03">
							<span class="instagram-info">
								 <span class="social-wrap">
									<span class="social-info">1
										<i class="pe-7s-chat"></i>
									</span>
									<span class="social-info">0
										<i class="pe-7s-like2"></i>
									</span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
