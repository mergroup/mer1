<!DOCTYPE html>
<html lang="en">

	@include('backend.layouts.head')

<body>
	@include('backend.layouts.loader')

	@include('backend.layouts.navbar')

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        @include('backend.layouts.sidebar')

        @yield('content')


    </div>
    <!-- END MAIN CONTAINER -->

    @include('backend.layouts.js')

</body>

</html>
