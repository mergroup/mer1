<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class CategoryController extends Controller
	{
		public function getAllCategory()
		{
			return view('backend.category.index');
		}

		public function getCategorySite()
		{
			return view('site.category.index');
		}
	}
