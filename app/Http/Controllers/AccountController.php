<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class AccountController extends Controller
	{
		public function getAccount()
		{
			return view('site.account.index');
		}

		public function getLogin()
		{
			return view('site.account.login-account');
		}

		public function getLostPassword()
		{
			return view('site.account.lostpassword');
		}
	}
