<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class WishlistController extends Controller
	{
		public function getWishlist()
		{
			return view('site.wishlist.index');
		}
	}
