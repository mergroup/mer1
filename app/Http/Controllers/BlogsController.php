<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class BlogsController extends Controller
	{
		public function getAllBlogs()
		{
			return view('site.blogs.index');
		}
	}
