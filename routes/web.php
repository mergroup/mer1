<?php

	use Illuminate\Support\Facades\Route;
	use App\Http\Controllers\DashboardController;
	use App\Http\Controllers\CategoryController;
	use App\Http\Controllers\BlogsController;
	use App\Http\Controllers\CustomerController;
	use App\Http\Controllers\AccountController;
	use App\Http\Controllers\WishlistController;
	use App\Http\Controllers\CartController;
	use App\Http\Controllers\CheckoutController;
	use App\Http\Controllers\OrdersController;
	use App\Http\Controllers\ProductsController;

	Route::get('/', function () {
			return view('site.index');
	});

	Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [DashboardController::class, 'getDashboard'])->name('dashboard.all');

	Route::middleware(['auth:sanctum', 'verified'])->get('/category/all', [CategoryController::class, 'getAllCategory'])->name('category.all');
	Route::get('/category', [CategoryController::class, 'getCategorySite'])->name('category');

	Route::get('blogs', [BlogsController::class, 'getAllBlogs'])->name('blogs.all');

	Route::get('customer', [CustomerController::class, 'getCustomer'])->name('customer');

	Route::get('account', [AccountController::class, 'getAccount'])->name('account');
	Route::get('login-account', [AccountController::class, 'getLogin'])->name('login-account');
	Route::get('lostpassword', [AccountController::class, 'getLostPassword'])->name('lostpassword');

	Route::get('wishlist', [WishlistController::class, 'getWishlist'])->name('wishlist');

	Route::get('cart', [CartController::class, 'getCart'])->name('cart');

	Route::get('checkout', [CheckoutController::class, 'getCheckout'])->name('checkout');

	Route::get('orders', [OrdersController::class, 'getOrders'])->name('orders');

	Route::get('products', [ProductsController::class, 'getProducts'])->name('products');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
